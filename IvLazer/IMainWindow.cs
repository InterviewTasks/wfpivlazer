﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace IvLazer
{
    public interface IMainWindow
    {
        void MainWindowWaitRun(Boolean WithAutoCounter);
        void MainWindowWaitSetPB(Int32 counter);
        Int32 MainWindowWaitGetPB();
        void MainWindowWaitCancel();

        void MainWindowUserWaitRun();
        void MainWindowUserWaitCancel();

        System.Windows.Media.Visual GetAsControl();

        void MainWindowMessageShow(String message);
        void MainWindowMessageCancel();
    }
}
