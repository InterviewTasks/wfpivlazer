﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Sounds
{
    public interface IPlayManager
    {
        void Stop(IPlaySound playSound);
        void Play(IPlaySound playSound);
    }
}
