﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.ComponentModel;
using System.Windows.Threading;

namespace IvLazer.Sounds
{
    enum EnumStatusSoundPlay
    {
        None,
        Stop,
        Play
    }

    /// <summary>
    /// Логика взаимодействия для PlaySound.xaml
    /// </summary>
    public partial class PlaySound : UserControl, IPlaySound, IDisposable
    {
        private IPlayManager PlayManager = null;

        private NAudio.Wave.WaveStream provider = null;
        private NAudio.Wave.WaveOutEvent waveOut = null;
        private BackgroundWorker backgroundWorker;
        DispatcherTimer _timer = null;

        public PlaySound()
        {
            InitializeComponent();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (waveOut != null)
                    waveOut.Dispose();
                if (provider != null)
                    provider.Dispose();
                if (backgroundWorker != null)
                    backgroundWorker.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public PlaySound(IPlayManager iPlayManager)
        {
            PlayManager = iPlayManager;

            InitializeComponent();

            StatusSoundPlay = EnumStatusSoundPlay.None;
            pb_percent.Value = 0;
            pb_percent.Minimum = 0;
            pb_percent.Maximum = 100;
            lb_percent.Content = "";
        }

        void IPlaySound.SetSoundData(Byte[] value)
        {
            SetSoundData(value);
        }

        protected void SetSoundData(Byte[] value)
        {
            StatusSoundPlay = EnumStatusSoundPlay.Stop;

            _timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(200)
            };
            _timer.Tick += new EventHandler(_timer_Tick);

            backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += OnDoWork;
            backgroundWorker.WorkerReportsProgress = false;
            backgroundWorker.RunWorkerCompleted += OnRunWorkerCompleted;

            provider = new NAudio.Wave.RawSourceWaveStream(
                         new MemoryStream(value), new NAudio.Wave.WaveFormat());
            waveOut = new NAudio.Wave.WaveOutEvent();
            waveOut.Init(provider);
            waveOut.PlaybackStopped += OnPlaybackStopped;
        }

        private EnumStatusSoundPlay _statusSoundPlay;
        private EnumStatusSoundPlay StatusSoundPlay
        {
            get
            {
                return _statusSoundPlay;
            }
            set
            {
                _statusSoundPlay = value;

                if (_statusSoundPlay == EnumStatusSoundPlay.None)
                {
                    btn_Play.IsEnabled = false;
                    btn_Play.Opacity = 0.3;
                    btn_Play.Background = new ImageBrush
                    {
                        ImageSource = ShareModuleStatic.Bitmap2BitmapImage(Properties.Resources.play),
                        Stretch = Stretch.None
                    };
                    lb_percent.Visibility = Visibility.Hidden;
                    pb_percent.Visibility = Visibility.Hidden;
                }
                else if (_statusSoundPlay == EnumStatusSoundPlay.Stop)
                {
                    btn_Play.IsEnabled = true;
                    btn_Play.Opacity = 1;
                    btn_Play.Background = new ImageBrush
                    {
                        ImageSource = ShareModuleStatic.Bitmap2BitmapImage(Properties.Resources.play),
                        Stretch = Stretch.None
                    };
                    lb_percent.Visibility = Visibility.Hidden;
                    pb_percent.Visibility = Visibility.Hidden;
                }
                else if (_statusSoundPlay == EnumStatusSoundPlay.Play)
                {
                    btn_Play.IsEnabled = true;
                    btn_Play.Opacity = 1;
                    btn_Play.Background = new ImageBrush
                    {
                        ImageSource = ShareModuleStatic.Bitmap2BitmapImage(Properties.Resources.stop),
                        Stretch = Stretch.None
                    };
                    lb_percent.Visibility = Visibility.Visible;
                    pb_percent.Visibility = Visibility.Visible;
                }
            }
        }

        private void Btn_Play_Click(object sender, RoutedEventArgs e)
        {
            if (StatusSoundPlay == EnumStatusSoundPlay.Stop)
            {
                if (waveOut != null)
                {
                    if (waveOut.PlaybackState != NAudio.Wave.PlaybackState.Playing)
                    {
                        PlayManager.Play((IPlaySound)this);
                        ShowCurrentTime(0);
                        provider.CurrentTime = TimeSpan.FromTicks(0);
                        _timer.Start();
                        StatusSoundPlay = EnumStatusSoundPlay.Play;
                        backgroundWorker.RunWorkerAsync();
                    }
                }
            }
            else if (StatusSoundPlay == EnumStatusSoundPlay.Play)
            {
                PlayStop();
            }
        }

        void IPlaySound.PlayStop()
        {
            PlayStop();
        }

        protected void PlayStop()
        {
            if (waveOut != null)
            {
                if (waveOut.PlaybackState == NAudio.Wave.PlaybackState.Playing)
                {
                    StatusSoundPlay = EnumStatusSoundPlay.Stop;
                    waveOut.Stop();
                    PlayManager.Stop((IPlaySound)this);
                }
            }
        }

        private void OnDoWork(object sender, DoWorkEventArgs e)
        {
            waveOut.Play();
        }

        private void OnPlaybackStopped(object sender, NAudio.Wave.StoppedEventArgs e)
        {
            _timer.Stop();
            PlayManager.Stop((IPlaySound)this);
            StatusSoundPlay = EnumStatusSoundPlay.Stop;
        }

        private void OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            pb_percent.Tag = provider.CurrentTime.Ticks;
            pb_percent.Value = 100 * provider.CurrentTime.Ticks / provider.TotalTime.Ticks;
            _timer.Start();
        }

        private void Pb_percent_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ShowCurrentTime((long)pb_percent.Tag);
        }

        private void ShowCurrentTime(long value)
        {
            lb_percent.Content = String.Format(@"{0:mm\:ss\.fff}", TimeSpan.FromTicks(value));
        }
    }
}