﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IvLazer.Sounds
{
    /// <summary>
    /// Логика взаимодействия для SoundsControl.xaml
    /// </summary>
    public partial class SoundsControl : UserControl, ISoundsControl
    {
        private IMainWindow iMainWindow;
        private IAppData appData;
        IPlayManager playManager;

        public SoundsControl()
        {
            InitializeComponent();
        }

        public SoundsControl(IMainWindow piMainWindow, IAppData pappData)
        {
            InitializeComponent();
            iMainWindow = piMainWindow;
            appData = pappData;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            playManager = new PlayManager();

            AppDbContext appDb = appData.GetDbContext();

            foreach(var s in appData.GetDbContext().Sound)
            {
                RowDefinition soundRowDefinition = new RowDefinition();

                SoundsGrid.RowDefinitions.Add(soundRowDefinition);
                SoundsGrid.RowDefinitions[SoundsGrid.RowDefinitions.Count - 1].Height = new GridLength(40);

                ResourceDictionary res = (ResourceDictionary)Application.LoadComponent(new Uri("DictionaryDesktop.xaml", UriKind.Relative));

                Label label_FileName = new Label
                {
                   Content = s.Name,
                   Style = (Style)res["SoundsGridLabel"]
                };
                SoundsGrid.Children.Add(label_FileName);
                Grid.SetRow(label_FileName, SoundsGrid.RowDefinitions.Count - 1);
                Grid.SetColumn(label_FileName, 0);

                Label label_FileSize = new Label
                {
                    Content = ShareModuleStatic.GetFileSize(s.Size),
                    Style = (Style)res["SoundsGridLabel"]
                };
                SoundsGrid.Children.Add(label_FileSize);
                Grid.SetRow(label_FileSize, SoundsGrid.RowDefinitions.Count - 1);
                Grid.SetColumn(label_FileSize, 1);

                PlaySound playSound = new PlaySound(playManager);
                SoundsGrid.Children.Add(playSound);
                Grid.SetRow(playSound, SoundsGrid.RowDefinitions.Count - 1);
                Grid.SetColumn(playSound, 3);

                FileDownload fileDownload = new FileDownload(playSound, s.Url, s.Size);
                SoundsGrid.Children.Add(fileDownload);
                Grid.SetRow(fileDownload, SoundsGrid.RowDefinitions.Count - 1);
                Grid.SetColumn(fileDownload, 2);
            }
        }

        Int32 SoundIndexPlay { set; get; } = -1;
    }
}
