﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Sounds
{
    public class PlayManager : IPlayManager
    {
        IPlaySound CurrentSound { get; set; } = null;

        public PlayManager()
        {
        }

        void IPlayManager.Stop(IPlaySound playSound)
        {
            Stop(playSound);
        }

        protected void Stop(IPlaySound playSound)
        {
            if (CurrentSound != null)
            {
                CurrentSound.PlayStop();
                CurrentSound = null;
            }
        }

        void IPlayManager.Play(IPlaySound playSound)
        {
            Play(playSound);
        }

        protected void Play(IPlaySound playSound)
        {
            if (CurrentSound != null)
            {
                CurrentSound.PlayStop();
            }
            CurrentSound = playSound;
        }
    }
}