﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Net;

namespace IvLazer.Sounds
{
    enum EnumStatusSoundDownload
    {
        None,
        Loading,
        Loaded
    }

    /// <summary>
    /// Логика взаимодействия для FileDownload.xaml
    /// </summary>
    public partial class FileDownload : UserControl
    {
        IPlaySound playSound;
        private String LoadUrl { get; set; } = "";
        private Int32 FileSize { get; set; } = 0;

        private WebClient Client { get; set; }

        public FileDownload()
        {
            InitializeComponent();
        }

        public FileDownload(IPlaySound iPlaySound, String loadUrl, Int32 size)
        {
            LoadUrl = loadUrl;
            FileSize = size;
            playSound = iPlaySound;

            InitializeComponent();

            StatusSoundDownload = EnumStatusSoundDownload.None;
            pb_percent.Value = 0;
            lb_percent.Content = "";

            Client = new WebClient();
            Client.DownloadProgressChanged += Client_DownloadProgressChanged;
            Client.DownloadDataCompleted += Client_DownloadDataCompleted;
        }

        private EnumStatusSoundDownload _statusSoundDownload;
        private EnumStatusSoundDownload StatusSoundDownload
        {
            get
            {
                return _statusSoundDownload;
            }
            set
            {
                _statusSoundDownload = value;

                if (StatusSoundDownload == EnumStatusSoundDownload.None)
                {
                    btn_Download.IsEnabled = true;
                    btn_Download.Background = new ImageBrush
                    {
                        ImageSource = ShareModuleStatic.Bitmap2BitmapImage(Properties.Resources.download_sound),
                        Stretch = Stretch.None
                    };
                    lb_fileloaded.Visibility = Visibility.Hidden;
                    lb_percent.Visibility = Visibility.Hidden;
                    pb_percent.Visibility = Visibility.Hidden;
                }
                else if (StatusSoundDownload == EnumStatusSoundDownload.Loading)
                {
                    btn_Download.IsEnabled = false;
                    btn_Download.Background = new ImageBrush
                    {
                        ImageSource = ShareModuleStatic.Bitmap2BitmapImage(Properties.Resources.downloaded_sound),
                        Stretch = Stretch.None
                    };
                    lb_fileloaded.Visibility = Visibility.Hidden;
                    lb_percent.Visibility = Visibility.Visible;
                    pb_percent.Visibility = Visibility.Visible;
                }
                else if (StatusSoundDownload == EnumStatusSoundDownload.Loaded)
                {
                    btn_Download.IsEnabled = false;
                    btn_Download.Background = new ImageBrush
                    {
                        ImageSource = ShareModuleStatic.Bitmap2BitmapImage(Properties.Resources.downloading_sound),
                        Stretch = Stretch.None
                    };
                    lb_fileloaded.Visibility = Visibility.Visible;
                    lb_percent.Visibility = Visibility.Hidden;
                    pb_percent.Visibility = Visibility.Hidden;
                }
            }
        }

        private void Btn_Download_Click(object sender, RoutedEventArgs e)
        {
            if (StatusSoundDownload == EnumStatusSoundDownload.None)
            {
                StatusSoundDownload = EnumStatusSoundDownload.Loading;
                Client.DownloadDataAsync(new System.Uri(LoadUrl));
            }
            else if (StatusSoundDownload == EnumStatusSoundDownload.Loading)
            {
                if (Client != null)
                    Client.CancelAsync();
            }
        }

        public void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs args)
        {
            pb_percent.Value = args.ProgressPercentage;
        }

        public void Client_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null)
            {
                // сравниваем размер полученных данных с огиринальным
                if (FileSize == e.Result.Length)
                {
                    playSound.SetSoundData(e.Result);
                    StatusSoundDownload = EnumStatusSoundDownload.Loaded;
                }
                else
                    StatusSoundDownload = EnumStatusSoundDownload.None;
            }
            else
                StatusSoundDownload = EnumStatusSoundDownload.None;
        }

        private void Pb_percent_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lb_percent.Content = String.Format("{0}%", pb_percent.Value);
        }
    }
}