﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Sounds
{
    public interface IPlaySound
    {
        void SetSoundData(Byte[] value);
        void PlayStop();
    }
}
