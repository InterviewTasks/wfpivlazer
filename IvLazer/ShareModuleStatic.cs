﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer
{
    public static class ShareModuleStatic
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (TimeZoneInfo.ConvertTimeToUtc(dateTime) -
                   new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
        }

        public static String GetFileSize(long SizeInByte)
        {
            String Result = String.Format("{0} Б", SizeInByte);
            if ((SizeInByte) < 1024)
                Result = SizeInByte + " Б";
            else if ((SizeInByte) < Math.Pow(1024, 2))
                Result = String.Format("{0:0} КБ", SizeInByte / Math.Pow(1024, 1));
            //            Result = String.Format("{0:0.00} КБ", SizeInByte / Math.Pow(1024, 1));
            else if ((SizeInByte) < Math.Pow(1024, 3))
                Result = String.Format("{0:0} МБ", SizeInByte / Math.Pow(1024, 2));
            //            Result = String.Format("{0:0.00} МБ", SizeInByte / Math.Pow(1024, 2));
            else if ((SizeInByte) < Math.Pow(1024, 4))
                Result = String.Format("{0:0} ГБ", SizeInByte / Math.Pow(1024, 3));
            //            Result = String.Format("{0:0.00} ГБ", SizeInByte / Math.Pow(1024, 3));
            else if ((SizeInByte) < Math.Pow(1024, 5))
                Result = String.Format("{0:0} ТБ", SizeInByte / Math.Pow(1024, 4));
            //            Result = String.Format("{0:0.00} ТБ", SizeInByte / Math.Pow(1024, 4));
            return Result;
        }

        public static System.Windows.Media.Imaging.BitmapImage Bitmap2BitmapImage(System.Drawing.Bitmap bitmap)
        {
            System.Windows.Media.Imaging.BitmapImage bitmapImage = new System.Windows.Media.Imaging.BitmapImage();
            using (System.IO.MemoryStream memory = new System.IO.MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                memory.Position = 0;
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
            }
            return bitmapImage;
        }
    }
}