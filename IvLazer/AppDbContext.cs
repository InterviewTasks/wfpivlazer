﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using IvLazer.Model;
using SQLite.CodeFirst;

namespace IvLazer
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
             : base("AppConnectionStringSqlite")
        {
        }

        public DbSet<ErrorItem> Error { get; set; }

        public DbSet<GameItem> Game { get; set; }
        public DbSet<TeamItem> Team { get; set; }
        public DbSet<PlayerItem> Player { get; set; }

        public DbSet<SoundItem> Sound { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<AppDbContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);
        }
    }

    public static class EntityExtensions
    {
        public static void Clear<T>(this DbSet<T> dbSet) where T : class
        {
            dbSet.RemoveRange(dbSet);
        }
    }
}