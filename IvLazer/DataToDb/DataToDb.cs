﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IvLazer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Xml.Serialization;
using System.IO;

namespace IvLazer.DataToDb
{
    public class DataToDb : IDataToDb
    {
        private IJsonView jsonView { get; set; } = null;
        private String DownloadJsonData { get; set; } = "";

        private Boolean DownloadResult { get; set; } = false;
        private String DownloadMessage
        {
            get
            {
                return DownloadResult ? "Файл успешно загружен" : "Файл не загружен";
            }
            set
            {
                DownloadMessage = value;
            }
        }

        public DataToDb() { }

        String IDataToDb.GetDownloadMessage()
        {
            return GetDownloadMessage();
        }

        protected String GetDownloadMessage()
        {
            return DownloadMessage;
        }

        Boolean IDataToDb.GetDownloadResult()
        {
            return GetDownloadResult();
        }

        protected Boolean GetDownloadResult()
        {
            return DownloadResult;
        }

        void IDataToDb.DownloadJson(String DownLoadlink)
        {
            DownloadJson(DownLoadlink);
        }

        protected void DownloadJson(String DownLoadlink)
        {
            Boolean Result = false;
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = System.Text.Encoding.UTF8;
                    DownloadJsonData = webClient.DownloadString(new System.Uri(DownLoadlink));
                    Result = true;
                }
            }
            catch (Exception) { }
            DownloadResult = Result;
        }

        JsonView IDataToDb.GetJsonObject()
        {
            return GetJsonObject();
        }

        protected JsonView GetJsonObject()
        {
            return jsonView.GetJsonViewObject();
        }

        void IDataToDb.DoLoad(IAppData pappData)
        {
            DoLoad(pappData);
        }

        protected void DoLoad(IAppData pAppData)
        {
            Boolean Result = false;
            jsonView = new JsonView();
            pAppData.ExecLoadedData(false);
            var appData = pAppData.GetDbContext();

            try
            {
                appData.Sound.Clear();
                appData.Player.Clear();
                appData.Team.Clear();
                appData.Game.Clear();
                appData.Error.Clear();

                JObject o = JObject.Parse(DownloadJsonData);

                String ErrorValue = (String)o.SelectToken("['error']");
                IEnumerable<JToken> oSounds = o.SelectToken("sounds");
                IEnumerable<JToken> oGames = o.SelectToken("games");

                // Error
                JToken oError = o.SelectToken("error");
                appData.Error.Add(new ErrorItem() { Name = ErrorValue });
                appData.SaveChanges();
                jsonView.SetError(ErrorValue);

                // Sound
                foreach (JToken item in oSounds)
                {
                    TempSound oItem = JsonConvert.DeserializeObject<TempSound>(JsonConvert.SerializeObject(item));
                    appData.Sound.Add(new SoundItem()
                    {
                        Name = oItem.Name,
                        Size = oItem.Size,
                        Url = oItem.Url
                    });
                    jsonView.AddSound(oItem.Name, oItem.Url, oItem.Size);
                }
                appData.SaveChanges();

                // Game
                foreach (JToken itemGameTemp in oGames)
                {
                    TempUrlGame oItem = JsonConvert.DeserializeObject<TempUrlGame>(JsonConvert.SerializeObject(itemGameTemp));
                    jsonView.AddGame(oItem.Url);
                    using (WebClient webClient = new WebClient())
                    {
                        webClient.Encoding = System.Text.Encoding.UTF8;
                        String xml = webClient.DownloadString(new System.Uri(oItem.Url));
                        // Create an instance of the XmlSerializer specifying type.
                        XmlSerializer serializer = new XmlSerializer(typeof(TempGame));
                        using (TextReader reader = new StringReader(xml))
                        {
                            TempGame tempGame = (TempGame)serializer.Deserialize(reader);
                            GameItem gameItem = new GameItem()
                            {
                                Url = oItem.Url,
                                Name = tempGame.Name,
                                Date = tempGame.Date
                            };
                            appData.Game.Add(gameItem);
                            appData.SaveChanges();
                            foreach (TempTeam tempTeam in tempGame.Teams)
                            {
                                TeamItem teamItem = new TeamItem()
                                {
                                    GameId = gameItem.Id,
                                    Name = tempTeam.Name
                                };
                                appData.Team.Add(teamItem);
                                appData.SaveChanges();
                                foreach (TempPlayer tempPlayer in tempTeam.Players)
                                {
                                    PlayerItem playerItem = new PlayerItem()
                                    {
                                        TeamId = teamItem.Id,
                                        Name = tempPlayer.Name,
                                        Rating = tempPlayer.Rating,
                                        Accuracy = tempPlayer.Accuracy,
                                        Shots = tempPlayer.Shots
                                    };
                                    appData.Player.Add(playerItem);
                                }
                                appData.SaveChanges();
                            }
                            appData.SaveChanges();
                        }
                    }
                }
                pAppData.ExecLoadedData(true);
                Result = true;
            }
            catch (Exception) { }

            DownloadResult = Result;
        }
    }
}