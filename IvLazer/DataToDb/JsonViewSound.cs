﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.DataToDb
{
    public class JsonViewSound
    {
        public JsonViewSound()
        {
        }

        public String Name { set; get; }
        public String Url { set; get; }
        public Int32 Size { set; get; }
    }
}
