﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IvLazer.DataToDb
{
    public class TempUrlGame
    {
        [JsonProperty(PropertyName = "url")]
        public String Url { get; set; }
    }
}
