﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.DataToDb
{
    public class JsonView : IJsonView
    {
        public JsonView()
        {
            error = null;
            games = new List<JsonViewGame>();
            sounds = new List<JsonViewSound>();
        }

        JsonView IJsonView.GetJsonViewObject()
        {
            return GetJsonViewObject();
        }

        protected JsonView GetJsonViewObject()
        {
            return this;
        }

        void IJsonView.SetError(String perror)
        {
            SetError(perror);
        }

        protected void SetError(String perror)
        {
            error = perror;
        }

        void IJsonView.AddGame(String url)
        {
            AddGame(url);
        }

        protected void AddGame(String url)
        {
            games.Add(new JsonViewGame()
            {
                Url = url
            });
        }

        void IJsonView.AddSound(String name, String url, Int32 size)
        {
            AddSound(name, url, size);
        }

        protected void AddSound(String name, String url, Int32 size)
        {
            sounds.Add(new JsonViewSound()
            {
                Name = name,
                Url = url,
                Size = size
            });
        }

        public string error { set; get; }
        public List<JsonViewSound> sounds { set; get; }
        public List<JsonViewGame> games { set; get; }
    }
}