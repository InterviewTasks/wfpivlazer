﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace IvLazer.DataToDb
{
    public interface IDataToDb
    {
        void DownloadJson(String DownLoadlink);
        void DoLoad(IAppData appData);

        JsonView GetJsonObject();

        String GetDownloadMessage();
        Boolean GetDownloadResult();
    }
}