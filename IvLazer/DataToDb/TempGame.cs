﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IvLazer.DataToDb
{
    [XmlRoot("game"), Serializable]
    public class TempGame
    {
        public TempGame()
        {
        }

        [XmlAttribute("name")]
        public String Name { get; set; }
        [XmlIgnore]
        public DateTime Date { get; set; }

        [XmlAttribute("date")]
        public Double DateUnix
        {
            get { return ShareModuleStatic.DateTimeToUnixTimestamp(Date); }
            set { Date = ShareModuleStatic.UnixTimeStampToDateTime(value); }
        }
        [XmlElement("team")]
        public TempTeam[] Teams { get; set; }
    }

    public class TempTeam
    {
        public TempTeam()
        {
        }

        [XmlAttribute("name")]
        public String Name { get; set; }
        [XmlElement("player")]
        public TempPlayer[] Players { get; set; }
}

    public class TempPlayer
    {
        public TempPlayer()
        {
        }

        [XmlAttribute("name")]
        public String Name { get; set; }
        [XmlAttribute("rating")]
        public Int32 Rating { get; set; }
        [XmlAttribute("accuracy")]
        public Decimal Accuracy { get; set; }
        [XmlAttribute("shots")]
        public Int32 Shots { get; set; }
    }
}
