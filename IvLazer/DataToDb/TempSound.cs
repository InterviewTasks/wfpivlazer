﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IvLazer.DataToDb
{
    public class TempSound
    {
        [JsonProperty(PropertyName = "name")]
        public String Name { get; set; }
        [JsonProperty(PropertyName = "url")]
        public String Url { get; set; }
        [JsonProperty(PropertyName = "size")]
        public int Size { get; set; }
    }
}
