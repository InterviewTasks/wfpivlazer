﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.DataToDb
{
    public interface IJsonView
    {
        JsonView GetJsonViewObject();
        void SetError(String error);
        void AddGame(String url);
        void AddSound(String name, String url, Int32 size);
    }
}
