﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.ObjectModel;

namespace IvLazer.Model
{
    // {"error": null или "текст ошибки", "games": [ { "url": "URL для загрузки xml-файла"} ], "sounds": [ { "name": "название", "url": "URL для загрузки", size: int(байты) } }

    [Table("Error")]
    public class ErrorItem
    {
        [Key, Column("Id")]
        public Int32 Id { get; set; }

        [Column("Name")]
        public String Name { get; set; }
    }

    [Table("Game")]
    public class GameItem
    {
        [Key, Column("Id")]
        public Int32 Id { get; set; }

        [Column("Url")]
        public String Url { get; set; }

        [Column("Name")]
        public String Name { get; set; }

        [Column("Date")]
        public DateTime Date { get; set; }

        // Ссылка на Team
        //public List<TeamItem> Team { get; set; }
        public ObservableCollection<TeamItem> Team { get; set; }

        public GameItem()
        {
            Team = new ObservableCollection<TeamItem>();
        }
    }

    [Table("Team")]
    public class TeamItem
    {
        [Key, Column("Id")]
        public Int32 Id { get; set; }

        // Вторичный ключ
        public virtual GameItem Game { get; set; }
        [Column("GameId"), ForeignKey("Game")]
        public Int32 GameId { get; set; }

        [Column("Name")]
        public String Name { get; set; }

        // Ссылка на Player
        //public List<PlayerItem> Player { get; set; }
        public ObservableCollection<PlayerItem> Player { get; set; }

        public TeamItem()
        {
            Player = new ObservableCollection<PlayerItem>();
        }
    }

    [Table("Player")]
    public class PlayerItem
    {
        [Key, Column("Id")]
        public Int32 Id { get; set; }

        // Вторичный ключ
        public virtual TeamItem Team { get; set; }
        [Column("TeamId"), ForeignKey("Team")]
        public Int32 TeamId { get; set; }

        [Column("Name")]
        public String Name { get; set; }

        [Column("Rating")]
        public Int32 Rating { get; set; }

        [Column("Accuracy")]
        public Decimal Accuracy { get; set; }

        [Column("Shots")]
        public Int32 Shots { get; set; }
    }

    [Table("Sound")]
    public class SoundItem
    {
        [Key, Column("Id")]
        public Int32 Id { get; set; }

        [Column("Url")]
        public String Url { get; set; }

        [Column("Name")]
        public String Name { get; set; }

        [Column("Size")]
        public Int32 Size { get; set; }
    }
}