﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IvLazer.Download
{
    /// <summary>
    /// Логика взаимодействия для DownloadControl.xaml
    /// </summary>
    public partial class DownloadControl : UserControl, IDownloadControl, IDisposable
    {
        IMainWindow iMainWindow;
        IAppData appData;

        private BackgroundWorker backgroundWorker;

        public DownloadControl()
        {
            InitializeComponent();
        }

        public DownloadControl(IMainWindow piMainWindow, IAppData pappData)
        {
            InitializeComponent();
            iMainWindow = piMainWindow;
            appData = pappData;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (backgroundWorker != null)
                    backgroundWorker.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += backgroundWorker_DoWork;
            backgroundWorker.WorkerReportsProgress = false;
            backgroundWorker.RunWorkerCompleted += backgroundWorker_RunWorkerCompleted;

            lbl_Message.Content = "";
            txt_HtmlLink.Text = "https://laserwar.com/testtask/get?dataType=Json";
        }

        private void Btn_LoadData_Click(object sender, RoutedEventArgs e)
        {
            // Запуск окна ожидания
            iMainWindow.MainWindowWaitRun(true);
            backgroundWorker.RunWorkerAsync(txt_HtmlLink.Text);
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DataToDb.IDataToDb dataToDb = new DataToDb.DataToDb();

            // загрузить json по ссылке
            dataToDb.DownloadJson(e.Argument.ToString());
            if (dataToDb.GetDownloadResult())
                dataToDb.DoLoad(appData);
            e.Result = dataToDb;
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataToDb.IDataToDb dataToDb = (DataToDb.IDataToDb)e.Result;

            if (dataToDb.GetDownloadResult())
                Set_tv_Result(dataToDb.GetJsonObject());
            else
                Clear_tv_Result();

            Set_lbl_Message(dataToDb.GetDownloadMessage());
            // Закрыть окно ожидания
            iMainWindow.MainWindowWaitCancel();
        }

        delegate void MethodInvokerClear_tv_Result();
        void Clear_tv_Result()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new MethodInvokerClear_tv_Result(Clear_tv_Result), null);
                return;
            }
            tv_Result.Items.Clear();
        }

        delegate void MethodInvokerSet_tv_Result(DataToDb.JsonView arg);
        void Set_tv_Result(DataToDb.JsonView arg)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new MethodInvokerSet_tv_Result(Set_tv_Result), arg);
                return;
            }
            tv_Result.Items.Clear();
            var item_error = new TreeViewItem()
            {
                Header = "error=" + arg.error
            };
            tv_Result.Items.Add(item_error);

            var item_sounds = new TreeViewItem()
            {
                Header = "sounds"
            };
            tv_Result.Items.Add(item_sounds);
            foreach (DataToDb.JsonViewSound item in arg.sounds)
            {
                var item_sound = new TreeViewItem()
                {
                    Header = item.Name
                };
                item_sounds.Items.Add(item_sound);
                var item_url = new TreeViewItem()
                {
                    Header = String.Format("Url: {0}", item.Url)
                };
                item_sound.Items.Add(item_url);
                var item_size = new TreeViewItem()
                {
                    Header = String.Format("Size: {0}", item.Size)
                };
                item_sound.Items.Add(item_size);
            }

            var item_games = new TreeViewItem()
            {
                Header = "games"
            };
            tv_Result.Items.Add(item_games);
            foreach (DataToDb.JsonViewGame item in arg.games)
            {
                var item_game = new TreeViewItem()
                {
                    Header = item.Url
                };
                item_games.Items.Add(item_game);
            }
        }

        delegate void MethodInvokerSet_lbl_Message(String arg);
        void Set_lbl_Message(String arg)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new MethodInvokerSet_lbl_Message(Set_lbl_Message), arg);
                return;
            }
            lbl_Message.Content = arg;
        }
    }
}