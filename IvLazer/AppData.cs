﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer
{
    public class AppData : IAppData
    {
        private Boolean LoadedData { get; set; } = false;
        private AppDbContext AppDbInner { get; set; } = null;

        public AppData()
        {
            AppDbInner = new AppDbContext();
        }

        void IAppData.ExecLoadedData(Boolean loadData)
        {
            ExecLoadedData(loadData);
        }

        protected void ExecLoadedData(Boolean loadData)
        {
            LoadedData = loadData;
        }

        AppDbContext IAppData.GetDbContext()
        {
            return GetDbContext();
        }

        protected AppDbContext GetDbContext()
        {
            return AppDbInner;
        }
    }
}
