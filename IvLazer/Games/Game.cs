﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Games
{
    public class Game
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public DateTime Date { get; set; }
        public Int32 Qty { get; set; }
    }
}
