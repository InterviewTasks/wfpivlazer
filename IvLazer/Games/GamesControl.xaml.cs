﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace IvLazer.Games
{
    /// <summary>
    /// Логика взаимодействия для DownloadControl.xaml
    /// </summary>
    public partial class GamesControl : UserControl, IGamesControl, IDisposable
    {
        private IMainWindow iMainWindow;

        private IAppData appData = null;
        AppDbContext appDbContext = null;

        List<Game> viewGames;

        const String NamePdfFile = @"vk_pdf.pdf";
        Vk.IPosting2VK posting2VK;

        public GamesControl()
        {
            InitializeComponent();
        }

        public GamesControl(IMainWindow piMainWindow, IAppData pappData)
        {
            InitializeComponent();
            iMainWindow = piMainWindow;
            appData = pappData;
            appDbContext = appData.GetDbContext();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (posting2VK != null)
                    ((Vk.Posting2VK)posting2VK).Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            viewGames = appDbContext
                .Game
                .Select(g => new Game
                {
                    Id = g.Id,
                    Name = g.Name,
                    Date = g.Date,
                    Qty = g.Team.Sum(t => t.Player.Count)
                }).ToList();
            listview_game.ItemsSource = viewGames;
        }

        private void Listview_game_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listview_game.SelectedItem != null)
            {
                DataGridGame.ItemsSource = null;

                Int32 GameId = viewGames[listview_game.SelectedIndex].Id;
                lbl_GameName.Content = viewGames[listview_game.SelectedIndex].Name;

                DataGridGame.ItemsSource = appDbContext
                   .Player
                   .Where(x => x.Team.Game.Id == GameId)
                   .Select(p => p).ToList();

                ICollectionView cvs = CollectionViewSource.GetDefaultView(DataGridGame.ItemsSource);

                cvs.GroupDescriptions.Add(new PropertyGroupDescription("Team.Name"));

                cvs.SortDescriptions.Add(new SortDescription("Name",
                    ListSortDirection.Ascending));
                cvs.SortDescriptions.Add(new SortDescription("Rating",
                    ListSortDirection.Ascending));
                cvs.SortDescriptions.Add(new SortDescription("Accuracy",
                    ListSortDirection.Ascending));
                cvs.SortDescriptions.Add(new SortDescription("Shots",
                    ListSortDirection.Ascending));

                TabControlGame.SelectedItem = TabItemEdit;
            }
        }

        private void Btn_Back_Click(object sender, RoutedEventArgs e)
        {
            TabControlGame.SelectedItem = TabItemList;
        }

        DataGridColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            ListSortDirection direction;

            if (e.OriginalSource is DataGridColumnHeader headerClicked)
            {
                if (headerClicked != _lastHeaderClicked)
                    direction = ListSortDirection.Ascending;
                else
                    direction =
                    (_lastDirection == ListSortDirection.Ascending) ?
                    ListSortDirection.Descending :
                    ListSortDirection.Ascending;

                headerClicked.Column.HeaderStyle =
                    (direction == ListSortDirection.Ascending) ?
                    Resources["GridHeaderGameDetail_Asc"] as Style :
                    Resources["GridHeaderGameDetail_Desc"] as Style;

                // Remove arrow from previously sorted header 
                if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
                {
                    _lastHeaderClicked.Column.HeaderStyle =
                      Resources["GridHeaderGameDetail_None"] as Style;
                }
                _lastHeaderClicked = headerClicked;
                _lastDirection = direction;
            }
        }

        private DataGridCell CellEditingLast = null;

        public DataGridCell GetDataGridCell(DataGridCellInfo cellInfo)
        {
            var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);
            if (cellContent != null)
                return (DataGridCell)cellContent.Parent;
            return null;
        }

        private void DataGridGame_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            CellEditingLast = GetDataGridCell(DataGridGame.CurrentCell);
        }

        private void DataGridGame_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
                appDbContext.SaveChanges();
        }

        public Player rowBeingEdited = null;

        private void DataGridGame_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedRowObject = DataGridGame.CurrentItem;
            if (selectedRowObject != null)
            {
                Model.PlayerItem selectedPlayerItem = (Model.PlayerItem)selectedRowObject;
                rowBeingEdited = new Player()
                {
                    Name = selectedPlayerItem.Name,
                    Rating = selectedPlayerItem.Rating,
                    Accuracy = selectedPlayerItem.Accuracy,
                    Shots = selectedPlayerItem.Shots
                };
                EditDetail.DataContext = rowBeingEdited;

                if (CellEditingLast != null || CellEditingLast.IsEditing)
                    DataGridGame.CancelEdit();

                // блокировка экрана
                iMainWindow.MainWindowUserWaitRun();
                // переход на закладку
                TabControlGame.SelectedItem = TabItemDetail;
            }
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            rowBeingEdited = null;
            
            iMainWindow.MainWindowUserWaitCancel();
            TabControlGame.SelectedItem = TabItemEdit;
        }

        private void Btn_Save_Click(object sender, RoutedEventArgs e)
        {
            Model.PlayerItem selectedPlayerItem = (Model.PlayerItem)DataGridGame.CurrentItem;
            if (selectedPlayerItem != null)
            {

                DataGridGame.BeginEdit();

                selectedPlayerItem.Name= rowBeingEdited.Name;
                selectedPlayerItem.Rating = rowBeingEdited.Rating;
                selectedPlayerItem.Accuracy = rowBeingEdited.Accuracy;
                selectedPlayerItem.Shots = rowBeingEdited.Shots;

                DataGridGame.CommitEdit();
                appDbContext.SaveChanges();
                DataGridGame.CommitEdit();
            }
            rowBeingEdited = null;

            iMainWindow.MainWindowUserWaitCancel();
            TabControlGame.SelectedItem = TabItemEdit;
        }

        private void Btn_SaveToPdf_Click(object sender, RoutedEventArgs e)
        {
            var result = new Print.FlowDocumentSaveAsPdf().Do(
                     new Print.PrintTask001.PrintTaskParameters()
                     {
                         GameId = viewGames[listview_game.SelectedIndex].Id,
                         appDbContext = this.appDbContext,
                         PdfFileName = null
                     });
            if (result.Result == Print.PrintResultType.Fail)
            {
                // вызвать окно ошибки

            }
        }

        // Точка входа в "ПУБЛИКАЦИЯ В ВК" (это авторизация в ВК)
        private void Btn_PubVK_Click(object sender, RoutedEventArgs e)
        {
            VkGroupList.ItemsSource = null;
            VkMessage.Text = String.Empty;
            posting2VK = new Vk.Posting2VK();
            TabControlGame.SelectedItem = TabItemVkAuth;
        }

        // Возврат из "ПУБЛИКАЦИЯ В ВК"
        private void Btn_VkAuthkBack_Click(object sender, RoutedEventArgs e)
        {
            TabControlGame.SelectedItem =  TabItemEdit;
        }
        
        private void Btn_VkAuth_Click(object sender, RoutedEventArgs e)
        {
            // Выполнить авторизацияю
            if (posting2VK != null)
            {
                Vk.IPosting2VKResult vKResult;
                vKResult = null;
                // выполнить авторизавация
                posting2VK.Auth(VkLogin.Text, VkPassword.Text, out vKResult);
                if (vKResult.GetResult() == EnumResult.Success)
                {
                    // получить группы для выбора
                    vKResult = null;
                    VkNet.Utils.VkCollection<VkNet.Model.Group> groupList = posting2VK.GroupList(out vKResult);
                    if (vKResult.GetResult() == EnumResult.Success)
                    {
                        VkGroupList.ItemsSource = groupList;
                        TabControlGame.SelectedItem = TabItemVkPublic;
                    }
                }
                else
                {
                    // Вывод ошибки
                    iMainWindow.MainWindowMessageShow(String.Format("{0}{1}{2} | {3}",
                        vKResult.GetMessage(),
                        Environment.NewLine + Environment.NewLine,
                        vKResult.GetException().Message,
                        vKResult.GetException().StackTrace
                        ));
                }
            }
        }

        // Публикация в ВК
        private void Btn_VkPublic_Click(object sender, RoutedEventArgs e)
        {
            if (VkGroupList.SelectedItem != null)
            {
                if (posting2VK != null)
                {
                    var result = new Print.FlowDocumentSaveAsPdf().Do(
                             new Print.PrintTask001.PrintTaskParameters()
                             {
                                 GameId = viewGames[listview_game.SelectedIndex].Id,
                                 appDbContext = this.appDbContext,
                                 PdfFileName = NamePdfFile
                             });

                    if (result.Result == Print.PrintResultType.Success)
                    {
                        Visual mW = iMainWindow.GetAsControl();
                        Vk.IPosting2VKResult vKResult;
                        vKResult = null;
                        posting2VK.Post(
                            VkMessage.Text,
                            (VkNet.Model.Group)VkGroupList.SelectedItem,
                            mW,
                            NamePdfFile,
                            out vKResult);
                        if (vKResult.GetResult() == EnumResult.Success)
                        {
                            // вызвать окно ошибки
                            iMainWindow.MainWindowMessageShow(String.Format("{0}",
                                "Сообщение опубликовано"
                                ));
                            TabControlGame.SelectedItem = TabItemEdit;
                        }
                        else
                        {
                            iMainWindow.MainWindowMessageShow(String.Format("{0}{1}{2} | {3}",
                                vKResult.GetMessage(),
                                Environment.NewLine + Environment.NewLine,
                                vKResult.GetException().Message,
                                vKResult.GetException().StackTrace
                                ));
                        }
                    }
                    else
                    {
                        // вызвать окно ошибки
                        iMainWindow.MainWindowMessageShow(String.Format("{0}{1}{2} | {3}",
                            result.Message,
                            Environment.NewLine + Environment.NewLine,
                            result.exception.Message,
                            result.exception.StackTrace
                            ));
                    }
                }
            }
        }

        // Возврат на авторизацию
        private void Btn_VkPublicBack_Click(object sender, RoutedEventArgs e)
        {
            TabControlGame.SelectedItem = TabItemVkAuth;
        }

        private void Btn_VkCancel_Click(object sender, RoutedEventArgs e)
        {
            posting2VK = null;
            TabControlGame.SelectedItem = TabItemEdit;
        }
    }
}