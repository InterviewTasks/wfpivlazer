﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace IvLazer.Games
{
    public class Player : INotifyPropertyChanged, IEditableObject
    {
        // Private data.
        private String m_Name = String.Empty;
        private Int32 m_Rating = 0;                // Рейтинг
        private Decimal m_Accuracy = 0;            // Точность
        private Int32 m_Shots = 0;                 // Кол-во выстрелов

        // Public properties. 
        public string Name
        {
            get { return this.m_Name; }
            set
            {
                if (value != this.m_Name)
                {
                    this.m_Name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }
        public Int32 Rating
        {
            get { return this.m_Rating; }
            set
            {
                if (value != this.m_Rating)
                {
                    this.m_Rating = value;
                    NotifyPropertyChanged("Rating");
                }
            }
        }
        public Decimal Accuracy
        {
            get { return this.m_Accuracy; }
            set
            {
                if (value != this.m_Accuracy)
                {
                    this.m_Accuracy = value;
                    NotifyPropertyChanged("Accuracy");
                }
            }
        }
        public Int32 Shots
        {
            get { return this.m_Shots; }
            set
            {
                if (value != this.m_Shots)
                {
                    this.m_Shots = value;
                    NotifyPropertyChanged("Shots");
                }
            }
        }

        // The Player class implements INotifyPropertyChanged and IEditableObject 
        // so that the datagrid can properly respond to changes to the 
        // data collection and edits made in the DataGrid.

        // Data for undoing canceled edits.
        private Player temp_Player = null;
        private bool m_Editing = false;

        // Implement INotifyPropertyChanged interface.
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // Implement IEditableObject interface.
        public void BeginEdit()
        {
            if (m_Editing == false)
            {
                temp_Player = this.MemberwiseClone() as Player;
                m_Editing = true;
            }
        }

        public void CancelEdit()
        {
            if (m_Editing == true)
            {
                this.Name = temp_Player.Name;
                this.Rating = temp_Player.Rating;
                this.Accuracy = temp_Player.Accuracy;
                this.Shots = temp_Player.Shots;
                m_Editing = false;
            }
        }

        public void EndEdit()
        {
            if (m_Editing == true)
            {
                temp_Player = null;
                m_Editing = false;
            }
        }
    }
}
