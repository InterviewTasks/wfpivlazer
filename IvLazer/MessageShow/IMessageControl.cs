﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.MessageShow
{
    interface IMessageControl
    {
        void SetValues(IMainWindow iMainWindow, String message);
    }
}
