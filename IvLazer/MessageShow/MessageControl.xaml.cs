﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IvLazer.MessageShow
{
    /// <summary>
    /// Логика взаимодействия для DownloadControl.xaml
    /// </summary>
    public partial class MessageControl : UserControl, IMessageControl
    {
        IMainWindow imainWindow = null;

        public MessageControl()
        {
            InitializeComponent();
        }

        void IMessageControl.SetValues(IMainWindow iMainWindow, String message)
        {
            imainWindow = iMainWindow;
            lbl_Info.Content = message;
        }

        private void Btn_Ok_Click(object sender, RoutedEventArgs e)
        {
            if (imainWindow != null)
                imainWindow.MainWindowMessageCancel();
        }
    }
}