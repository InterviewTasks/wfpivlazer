﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Model.RequestParams;
using VkNet.Utils;

namespace IvLazer.Vk
{
    public class Posting2VK : IPosting2VK, IDisposable
    {
        private VkNet.VkApi api;

        const String AlbumTitle = @"Мой альбом";
        const String NameImageFile = @"vk_sc.bmp";

        const String PdfTitle = @"Pdf файл";

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                api.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Posting2VK()
        {
            api = new VkNet.VkApi();
        }

        void IPosting2VK.Auth(String VkLogin, String VkPassword, out IPosting2VKResult posting2VKMessage)
        {
            Auth(VkLogin, VkPassword, out posting2VKMessage);
        }

        protected void Auth(String VkLogin, String VkPassword, out IPosting2VKResult posting2VKMessage)
        {
            posting2VKMessage = new Posting2VKResult();
            try
            {
                api.Authorize(new ApiAuthParams
                {
                    ApplicationId = 6950191,
                    Login = VkLogin,
                    Password = VkPassword,
                    Settings = Settings.All
                });
                if (api.IsAuthorized)
                    posting2VKMessage.SetResult(EnumResult.Success);
                else
                {
                    posting2VKMessage.SetResult(EnumResult.Fail);
                    posting2VKMessage.SetMessage("Авторизация не выполнилась");
                }
            }
            catch (Exception ex)
            {
                posting2VKMessage.SetResult(EnumResult.Fail);
                posting2VKMessage.SetMessage(ex.Message);
                posting2VKMessage.SetException(ex);
            }
        }

        VkCollection<Group> IPosting2VK.GroupList(out IPosting2VKResult posting2VKMessage)
        {
            return GroupList(out posting2VKMessage);
        }

        protected VkCollection<Group> GroupList(out IPosting2VKResult posting2VKMessage)
        {
            VkCollection<Group> Result = null;
            posting2VKMessage = new Posting2VKResult();
            try
            {
                Result = api.Groups.Get(new GroupsGetParams()
                {
                    Extended = true
                });
                posting2VKMessage.SetResult(EnumResult.Success);
            }
            catch (Exception ex)
            {
                posting2VKMessage.SetResult(EnumResult.Fail);
                posting2VKMessage.SetMessage(ex.Message);
                posting2VKMessage.SetException(ex);
            }
            return Result;
        }

        void IPosting2VK.Post(
            String VkMessage,
            VkNet.Model.Group group,
            Visual mW,
            String NamePdfFile,
            out IPosting2VKResult posting2VKMessage)
        {
            Post(
                VkMessage, 
                group,
                mW,
                NamePdfFile,
                out posting2VKMessage);
        }

        protected void Post(
            String VkMessage, 
            VkNet.Model.Group group,
            Visual mW,
            String NamePdfFile,
            out IPosting2VKResult posting2VKMessage)
        {
            PhotoAlbum photoAlbum = null;

            posting2VKMessage = new Posting2VKResult();
            try
            {

                CreateBitmapFromVisual(mW, NameImageFile);
                var albums = api.Photo.GetAlbums(new PhotoGetAlbumsParams
                {
                    OwnerId = api.UserId.Value
                });
                Boolean albumExists = false;
                foreach (var album in albums)
                {
                    if (album.Title == AlbumTitle)
                    {
                        albumExists = true;
                        photoAlbum = album;
                        break;
                    }
                }
                if (!albumExists)
                {
                    photoAlbum = api.Photo.CreateAlbum(new PhotoCreateAlbumParams()
                    {
                        Title = AlbumTitle
                    });
                }
                
                // Получить адрес сервера для загрузки.
                var uploadServerImages = api.Photo.GetUploadServer(photoAlbum.Id);
                // Загрузить файл
                var wc = new System.Net.WebClient();
                var responseFile = Encoding.ASCII.GetString(wc.UploadFile(uploadServerImages.UploadUrl, NameImageFile));
                // Сохранить загруженный файл
                var photos = api.Photo.Save(new PhotoSaveParams
                {
                    SaveFileResponse = responseFile,
                    AlbumId = photoAlbum.Id
                });

                // Получить адрес сервера для загрузки.
                var uploadServerDocs = api.Docs.GetUploadServer();
                // Загрузить файл
                var responseFileDocs = Encoding.ASCII.GetString(wc.UploadFile(uploadServerDocs.UploadUrl, NamePdfFile));
                // Сохранить загруженный файл
                var doc = api.Docs.Save(responseFileDocs, PdfTitle);

                WallPostParams wallPostParams = new WallPostParams
                {
                    OwnerId = -group.Id,
                    Attachments = new List<MediaAttachment>() { photos[0],  doc[0].Instance },
                    Message = VkMessage
                };
                api.Wall.Post(wallPostParams);
            }
            catch (Exception ex)
            {
                posting2VKMessage.SetException(ex);
                posting2VKMessage.SetMessage(ex.Message);
                posting2VKMessage.SetResult(EnumResult.Fail);
            }
        }

        public static void CreateBitmapFromVisual(Visual target, string fileName)
        {
            if (target == null || string.IsNullOrEmpty(fileName))
            {
                return;
            }

            System.Windows.Rect bounds = VisualTreeHelper.GetDescendantBounds(target);

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((Int32)bounds.Width, (Int32)bounds.Height, 96, 96, PixelFormats.Pbgra32);

            DrawingVisual visual = new DrawingVisual();

            using (DrawingContext context = visual.RenderOpen())
            {
                VisualBrush visualBrush = new VisualBrush(target);
                context.DrawRectangle(visualBrush, null, new System.Windows.Rect(new System.Windows.Point(), bounds.Size));
            }
            renderTarget.Render(visual);
            PngBitmapEncoder bitmapEncoder = new PngBitmapEncoder();
            bitmapEncoder.Frames.Add(BitmapFrame.Create(renderTarget));
            using (System.IO.MemoryStream ms0 = new System.IO.MemoryStream())
            {
                bitmapEncoder.Save(ms0);
                using (System.IO.Stream stm = System.IO.File.Create(fileName))
                {
                    ms0.Position = 0;
                    ms0.CopyTo(stm);
                }
            }
        }
    }
}