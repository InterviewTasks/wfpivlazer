﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Vk
{
    public interface IPosting2VKResult
    {
        EnumResult GetResult();
        void SetResult(EnumResult @enum);
        String GetMessage();
        void SetMessage(String @mess);
        Exception GetException();
        void SetException(Exception @ex);
    }
}