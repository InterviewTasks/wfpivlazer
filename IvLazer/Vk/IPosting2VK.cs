﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Vk
{
    public interface IPosting2VK
    {
        void Auth(String VkLogin, String VkPassword, out IPosting2VKResult posting2VKMessage);
        VkNet.Utils.VkCollection<VkNet.Model.Group> GroupList(out IPosting2VKResult posting2VKMessage);
        void Post(
            String VkMessagem,
            VkNet.Model.Group group,
            Visual mW,
            String NamePdfFile,
            out IPosting2VKResult posting2VKMessage);
    }
}
