﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Vk
{
    public class Posting2VKResult : IPosting2VKResult
    {
        EnumResult Result { get; set; }
        String Message { get; set; }
        Exception ExceptionInner { get; set; }

        EnumResult IPosting2VKResult.GetResult()
        {
            return GetResult();
        }

        protected EnumResult GetResult()
        {
            return Result;
        }

        void IPosting2VKResult.SetResult(EnumResult @enum)
        {
            SetResult(@enum);
        }

        protected void SetResult(EnumResult @enum)
        {
            Result = @enum;
        }

        String IPosting2VKResult.GetMessage()
        {
            return GetMessage();
        }

        protected String GetMessage()
        {
            return Message;
        }

        void IPosting2VKResult.SetMessage(String @mess)
        {
            SetMessage(@mess);
        }

        protected void SetMessage(String @mess)
        {
            Message = @mess;
        }

        Exception IPosting2VKResult.GetException()
        {
            return GetException();
        }

        protected Exception GetException()
        {
            return ExceptionInner;
        }

        void IPosting2VKResult.SetException(Exception ex)
        {
            SetException(ex);
        }

        protected void SetException(Exception ex)
        {
            ExceptionInner = ex;
        }
    }
}