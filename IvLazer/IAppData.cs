﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer
{
    public interface IAppData
    {
        void ExecLoadedData(Boolean loadData);
        AppDbContext GetDbContext();
    }
}
