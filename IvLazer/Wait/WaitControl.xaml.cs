﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IvLazer.Wait
{
    /// <summary>
    /// Логика взаимодействия для DownloadControl.xaml
    /// </summary>
    public partial class WaitControl : UserControl, IWaitControl
    {
        public WaitControl()
        {
            InitializeComponent();
        }

        void IWaitControl.SetValueProgressBar(Int32 counter)
        {
            SetValueProgressBar(counter);
        }

        private void SetValueProgressBar(Int32 counter)
        {
            SetValuePB(counter);
        }

        delegate void MethodInvokerSetValuePB(Int32 arg);
        void SetValuePB(Int32 arg)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new MethodInvokerSetValuePB(SetValuePB), arg);
                return;
            }
            RoundProgressBar.Value = arg;
        }

        delegate Int32 MethodInvokeGetValuePBr();
        Int32 GetValuePB()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new MethodInvokeGetValuePBr(GetValuePB));
                return 10;
            }
            return Convert.ToInt32(RoundProgressBar.Value);
        }

        Int32 IWaitControl.GetValueProgressBar()
        {
            return GetValueProgressBar();
        }

        private Int32 GetValueProgressBar()
        {
            return GetValuePB();
        }
    }
}
