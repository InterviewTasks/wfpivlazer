﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Wait
{
    interface IWaitControl
    {
        void SetValueProgressBar(Int32 counter);
        Int32 GetValueProgressBar(); // не используется
    }
}
