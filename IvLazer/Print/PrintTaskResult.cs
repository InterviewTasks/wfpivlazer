﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace IvLazer.Print
{
    public class PrintTaskResult
    {
        public String Message { get; set; }
        public PrintResultType Result { get; set; }
        public Exception exception { get; set; }
        public FlowDocument Document { get; set; }
    }
}
