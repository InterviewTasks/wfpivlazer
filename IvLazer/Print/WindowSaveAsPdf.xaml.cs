﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.IO.Packaging;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;

namespace IvLazer.Print
{
    /// <summary>
    /// Логика взаимодействия для WindowSaveAsPdf.xaml
    /// </summary>
    public partial class WindowSaveAsPdf : Window
    {
        public WindowSaveAsPdf()
        {
            InitializeComponent();
        }

        public WindowSaveAsPdf(FlowDocument flowDocument)
        {
            InitializeComponent();
            flowDocSViewer.Document = flowDocument;
        }
        private void Btn_SaveToPdf_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "Portable Document Format файлы (*.pdf)|*.pdf",
                FileName = "Document", // Default file name
                DefaultExt = ".pdf", // Default file extension
                FilterIndex = 2,
                RestoreDirectory = true
            };

            // Show save file dialog box
            Nullable<bool> result = saveFileDialog.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = saveFileDialog.FileName;

                using (var stream = new FileStream("doc.xps",FileMode.Create))
                {
                    using (var package = Package.Open(stream, FileMode.Create, FileAccess.ReadWrite))
                    {
                        using (var xpsDoc = new XpsDocument(package, CompressionOption.Maximum))
                        {
                            var rsm = new XpsSerializationManager(new XpsPackagingPolicy(xpsDoc), false);
                            var paginator = ((IDocumentPaginatorSource)flowDocSViewer.Document).DocumentPaginator;
                            paginator.PageSize = new Size(640, 900);
                            rsm.SaveAsXaml(paginator);
                            rsm.Commit();
                        }
                    }
                    stream.Position = 0;
                }
                try
                {
                    PdfSharp.Xps.XpsConverter.Convert("doc.xps", filename, 1);
                }
                catch  (Exception ex)
                {

                }
                Close();
            }
            else
            {

            }
        }

        private void Btn_Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
