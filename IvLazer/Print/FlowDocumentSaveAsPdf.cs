﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.IO.Packaging;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Windows.Xps.Serialization;

namespace IvLazer.Print
{
    public class FlowDocumentSaveAsPdf
    {
        // Имя временного файла
        private readonly String tempXpsFile = Guid.NewGuid().ToString().Replace("-", "") + ".xps";

        public  FlowDocumentSaveAsPdf()
        {
        }

        public PrintTaskResult Do(Print.PrintTask001.PrintTaskParameters printTaskParameters)
        {
            PrintTaskResult printTaskResult = new Print.PrintTask001.PrintTask(
                printTaskParameters).DoTask();
            if (printTaskResult.Result == Print.PrintResultType.Success)
            {
                printTaskResult.Result = PrintResultType.Fail;

                SaveFileDialog saveFileDialog = null;
                Nullable<bool> result = null;
                if (printTaskParameters.PdfFileName == null)
                {
                    saveFileDialog = new SaveFileDialog
                    {
                        Filter = "Portable Document Format файлы (*.pdf)|*.pdf",
                        FileName = "Document", // Default file name
                        DefaultExt = ".pdf", // Default file extension
                        FilterIndex = 2,
                        RestoreDirectory = true
                    };
                    // Show save file dialog box
                    result = saveFileDialog.ShowDialog();
                }
                // Process save file dialog box results
                if (result == true || printTaskParameters.PdfFileName != null)
                {
                    // Save document
                    string filename = "";
                    if (printTaskParameters.PdfFileName == null)
                        filename = saveFileDialog.FileName;
                    else
                        filename = printTaskParameters.PdfFileName;

                    FileStream stream = null;
                    try
                    {
                        stream = new FileStream(tempXpsFile, FileMode.Create);
                        using (var package = Package.Open(stream, FileMode.Create, FileAccess.ReadWrite))
                        {
                            using (var xpsDoc = new XpsDocument(package, CompressionOption.Maximum))
                            {
                                var rsm = new XpsSerializationManager(new XpsPackagingPolicy(xpsDoc), false);
                                var paginator = ((IDocumentPaginatorSource)printTaskResult.Document).DocumentPaginator;
                                paginator.PageSize = new Size(640, 900);
                                rsm.SaveAsXaml(paginator);
                                rsm.Commit();
                                printTaskResult.Result = PrintResultType.Success;
                            }
                        }
                    }
                    finally
                    {
                        if (stream != null)
                            stream.Dispose();
                    }
                    if (printTaskResult.Result == Print.PrintResultType.Success)
                    {
                        printTaskResult.Result = PrintResultType.Fail;
                        try
                        {
                            PdfSharp.Xps.XpsConverter.Convert(tempXpsFile, filename, 1);
                            printTaskResult.Result = PrintResultType.Success;
                        }
                        catch (Exception ex)
                        {
                            printTaskResult.Message = String.Format("Ошибка создания Pdf файла из {0}",tempXpsFile);
                            printTaskResult.exception = ex;
                        }
                    }
                    if (printTaskResult.Result == Print.PrintResultType.Success && File.Exists(tempXpsFile))
                    {
                        try
                        {
                            File.Delete(tempXpsFile);
                        }
                        catch (Exception ex)
                        {
                            printTaskResult.Message = String.Format("Ошибка удаления файл {0}", tempXpsFile);
                            printTaskResult.exception = ex;
                        }
                    }
                }
                else
                {
                    printTaskResult.Message = "Pdf файл не выбран";
                }
            }
            return printTaskResult;
        }
    }
}