﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Print.PrintTask001
{
    public class PrintTaskParameters
    {
        public Int32 GameId { set; get; }
        public AppDbContext appDbContext { set; get; }
        public String PdfFileName { set; get; }
    }
}
