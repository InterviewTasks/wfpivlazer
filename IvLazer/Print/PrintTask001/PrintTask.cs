﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Markup;
using System.IO;

namespace IvLazer.Print.PrintTask001
{
    public class PrintTask
    {
        private PrintTaskParameters parameters;

        public PrintTask(PrintTaskParameters p)
        {
            parameters = p;
        }

        public PrintTaskResult DoTask()
        {
            PrintTaskResult result = new PrintTaskResult()
            {
                Result = PrintResultType.Fail
            };

            // идентификатор игры
            var GameId = parameters.GameId;

            // нашли наименование события
            var game = parameters.appDbContext
               .Game
               .FirstOrDefault(x => x.Id == GameId);
            if (game == null)
            {
                throw new Exception("Не найден идентификатор игры, ситуация невозможна");
            }
            else
            {
                FlowDocument flowDocument = new FlowDocument();

                #region Data

                var players = parameters.appDbContext
                   .Player
                   .Where(x => x.Team.Game.Id == GameId)
                   .Select(p => p)
                   .OrderBy(p => p.Team.Name)
                   .ThenBy(p=> p.Name);

                Int32 DistinctCount = parameters.appDbContext
                   .Player
                   .Where(x => x.Team.Game.Id == GameId)
                   .Select(x => x.TeamId).Distinct().Count();

                var teamsStats = parameters.appDbContext
                   .Player
                   .Where(x => x.Team.Game.Id == GameId)
                   .GroupBy(l => l.TeamId)
                  .Select(g => new TeamStats()
                  {
                      TeamName = g.Select(l => l.Team.Name).FirstOrDefault(),
                      RatingAvg = g.Select(l => l.Rating).Average(),
                      AccuracyAvg = g.Select(l => l.Accuracy).Average(),
                  });
                
                #endregion Data

                #region flowDocument.Blocks.Header

                Paragraph paragraphHeader = new Paragraph();
                paragraphHeader.FontFamily = new System.Windows.Media.FontFamily("MS Sans Serif");
                paragraphHeader.FontSize = 24;
                paragraphHeader.FontWeight = FontWeights.Bold;
                paragraphHeader.Inlines.Add(new Bold(new Run(
                    String.Format("{0}  {1:dd.MM.yy}", game.Name, game.Date))));

                // Добавили заголовок
                flowDocument.Blocks.Add(paragraphHeader);

                #endregion flowDocument.Blocks.Header

                #region flowDocument.Blocks.Table

                // Create the Table...
                Table table = new Table();
                table.FontFamily = new System.Windows.Media.FontFamily("MS Sans Serif");
                table.FontSize = 12;
                table.FontWeight = FontWeights.Normal;

                // ...and add it to the FlowDocument Blocks collection.
                flowDocument.Blocks.Add(table);

                // Set some global formatting properties for the table.
                table.CellSpacing = 0;
                table.Background = System.Windows.Media.Brushes.White;

                for (int i = 0; i < 4; i++)
                {
                    table.Columns.Add(new TableColumn());
                    table.Columns[i].Width = new GridLength(150);
                }

                TableRow currentRow = null;
                // Группа заголовка
                int counterRowGroup = 0;
                int counterRow = 0;

                // Create and add an empty TableRowGroup to hold the table's Rows.
                table.RowGroups.Add(new TableRowGroup());
                table.RowGroups[counterRowGroup].Rows.Add(new TableRow());
                
                // Alias the current working row for easy reference.
                currentRow = table.RowGroups[counterRowGroup].Rows[0];

                // Global formatting for the header row.
                currentRow.FontWeight = FontWeights.Bold;

                // Add cells with content to the second row.
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Игрок"))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Рейтинг"))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Точность"))));
                currentRow.Cells.Add(new TableCell(new Paragraph(new Run("Выстрелы"))));

                Style styleTableCell = new Style(typeof(TableCell));
                styleTableCell.Setters.Add(new Setter(TableCell.BorderBrushProperty, System.Windows.Media.Brushes.LightGray));
                styleTableCell.Setters.Add(new Setter(TableCell.BorderThicknessProperty, new Thickness(0, 0, 0, 1)));
                styleTableCell.Setters.Add(new Setter(TableCell.PaddingProperty, new Thickness(0, 20, 0, 0)));

                Style styleTableCellGroup = new Style(typeof(TableCell));
                styleTableCellGroup.Setters.Add(new Setter(TableCell.BorderThicknessProperty, new Thickness(0, 0, 0, 0)));
                styleTableCellGroup.Setters.Add(new Setter(TableCell.PaddingProperty, new Thickness(0, 20, 0, 0)));

                var currentTeam = string.Empty;
                foreach (var player in players)
                {
                    if (currentTeam != player.Team.Name)
                    {
                        currentTeam = player.Team.Name;
                        counterRowGroup++;
                        counterRow = 0;

                        table.RowGroups.Add(new TableRowGroup());
                        table.RowGroups[counterRowGroup].Rows.Add(new TableRow());
                        currentRow = table.RowGroups[counterRowGroup].Rows[counterRow];

                        currentRow.Background = System.Windows.Media.Brushes.White;
                        currentRow.FontSize = 16;
                        currentRow.FontWeight = System.Windows.FontWeights.Bold;

                        // Add the header row with content, 
                        currentRow.Cells.Add(new TableCell(new Paragraph(new Run(player.Team.Name))));
                        // and set the row to span all 4 columns.
                        currentRow.Cells[0].ColumnSpan = 4;
                        currentRow.Cells[0].Style = styleTableCellGroup;
                    }
                    counterRow++;
                    table.RowGroups[counterRowGroup].Rows.Add(new TableRow());
                    currentRow = table.RowGroups[counterRowGroup].Rows[counterRow];

                    // Add cells with content to the third row.
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(player.Name))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(player.Rating.ToString()))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(player.Accuracy.ToString()))));
                    currentRow.Cells.Add(new TableCell(new Paragraph(new Run(player.Shots.ToString()))));
                    for (int i = 0; i < 4; i++)
                    {
                        currentRow.Cells[i].Style = styleTableCell;
                    }
                }
                #endregion flowDocument.Blocks.Table

                #region flowDocument.Blocks.Empty

                Paragraph paragraphSeparator = new Paragraph();
                paragraphSeparator.Inlines.Add(new LineBreak());
                flowDocument.Blocks.Add(paragraphSeparator);

                #endregion flowDocument.Blocks.Empty

                #region flowDocument.Blocks.Statistic

                Table tableStatistic = new Table();
                tableStatistic.FontFamily = new System.Windows.Media.FontFamily("MS Sans Serif");
                tableStatistic.FontSize = 12;
                tableStatistic.FontWeight = FontWeights.Normal;

                // ...and add it to the FlowDocument Blocks collection.
                flowDocument.Blocks.Add(tableStatistic);

                // Set some global formatting properties for the table.
                tableStatistic.CellSpacing = 20;
                tableStatistic.Background = System.Windows.Media.Brushes.White;

                for (int i = 0; i < DistinctCount; i++)
                {
                    tableStatistic.Columns.Add(new TableColumn());
                    tableStatistic.Columns[i].Width = new GridLength(480 / DistinctCount);
                }

                tableStatistic.RowGroups.Add(new TableRowGroup());
                tableStatistic.RowGroups[0].Rows.Add(new TableRow());
                currentRow = tableStatistic.RowGroups[0].Rows[0];

                ResourceDictionary res = (ResourceDictionary)Application.LoadComponent(new Uri("DictionaryDesktop.xaml", UriKind.Relative));
                foreach (TeamStats teamStats in teamsStats)
                {
                    StatsItem statsItem = new StatsItem(teamStats)
                    {
                        Style = (Style)res["StatItem"]
                    };
                    var lineBlock = new BlockUIContainer(statsItem);
                    currentRow.Cells.Add(new TableCell(lineBlock));
                }

                #endregion flowDocument.Blocks.Statistic

                #region flowDocument.Blocks.Empty

                //Paragraph paragraphSeparatorFooter = new Paragraph();
                //paragraphSeparatorFooter.Inlines.Add(new LineBreak());
                //flowDocument.Blocks.Add(paragraphSeparatorFooter);

                #endregion flowDocument.Blocks.Empty

                result.Document = flowDocument;
                result.Result = PrintResultType.Success;
            }
            return result;
        }
    }
}
