﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvLazer.Print.PrintTask001
{
    public class TeamStats
    {
        public String TeamName { get; set; } = String.Empty;
        public Double RatingAvg { get; set; } = 0;
        public Decimal AccuracyAvg { get; set; } = 0;
    }
}
