﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IvLazer.Print.PrintTask001
{
    /// <summary>
    /// Логика взаимодействия для StatsItem.xaml
    /// </summary>
    public partial class StatsItem : UserControl
    {
        public StatsItem()
        {
            InitializeComponent();
        }

        public StatsItem(TeamStats teamStats)
        {
            InitializeComponent();
            TeamName.Content = teamStats.TeamName;
            RatingValue.Content = String.Format("{0:0.#}", teamStats.RatingAvg);
            AccuracyValue.Content = String.Format("{0:P1}", teamStats.AccuracyAvg);

            if (teamStats.RatingAvg > 1000) { teamStats.RatingAvg = 1000; }
            if (teamStats.RatingAvg < 0) { teamStats.RatingAvg = 0; }
            RatingColumn0.Width = new GridLength(teamStats.RatingAvg, GridUnitType.Star);
            RatingColumn1.Width = new GridLength(1000- teamStats.RatingAvg, GridUnitType.Star);

            if (teamStats.AccuracyAvg > 1) { teamStats.AccuracyAvg = 1; }
            if (teamStats.AccuracyAvg < 0) { teamStats.AccuracyAvg = 0; }
            AccuracyColumn0.Width = new GridLength(1000*(Double)teamStats.AccuracyAvg, GridUnitType.Star);
            AccuracyColumn1.Width = new GridLength(1000 * (1 - (Double)teamStats.AccuracyAvg), GridUnitType.Star);
        }
    }
}
