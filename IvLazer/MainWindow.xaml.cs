﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using Unity.Container;
using System.Windows.Media.Animation;
using System.ComponentModel;
using Unity;

namespace IvLazer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IMainWindow, IDisposable
    {
        const String RegElementNameCustom = "REG_ELEMENT_CUSTOM";
        const String RegElementNameWait = "REG_ELEMENT_WAIT";
        const String RegElementNameError = "REG_ELEMENT_ERROR";
        const String RegElementNameEdit = "REG_ELEMENT_EDIT";

        IUnityContainer container;
        IAppData appData;

        private BackgroundWorker backgroundWorkerShowWaitWindowWithCounter;
        private BackgroundWorker backgroundWorkerShowWaitWindowWithAutoCounter;

        public MainWindow()
        {
            InitializeComponent();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (container != null)
                    container.Dispose();
                if (backgroundWorkerShowWaitWindowWithCounter != null)
                    backgroundWorkerShowWaitWindowWithCounter.Dispose();
                if (backgroundWorkerShowWaitWindowWithAutoCounter != null)
                    backgroundWorkerShowWaitWindowWithAutoCounter.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        Visual IMainWindow.GetAsControl()
        {
            return GetAsControl();
        }

        protected Visual GetAsControl()
        {
            return this;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            appData = new AppData();
            container = new Unity.UnityContainer();
            // Start first UI (default)
            RunAction("download");

            backgroundWorkerShowWaitWindowWithCounter = new BackgroundWorker();
            backgroundWorkerShowWaitWindowWithCounter.DoWork += BackgroundWorkerShowWaitWindowWithCounter_DoWork;
            backgroundWorkerShowWaitWindowWithCounter.WorkerReportsProgress = true;
            backgroundWorkerShowWaitWindowWithCounter.RunWorkerCompleted += BackgroundWorkerShowWaitWindow_RunWorkerCompleted;

            backgroundWorkerShowWaitWindowWithAutoCounter = new BackgroundWorker();
            backgroundWorkerShowWaitWindowWithAutoCounter.DoWork += BackgroundWorkerShowWaitWindowWithAutoCounter_DoWork;
            backgroundWorkerShowWaitWindowWithAutoCounter.WorkerReportsProgress = false;
            backgroundWorkerShowWaitWindowWithAutoCounter.RunWorkerCompleted += BackgroundWorkerShowWaitWindow_RunWorkerCompleted;

        }
        private void StackPanel_Click(object sender, RoutedEventArgs e)
        {
            RunAction(((Button)e.OriginalSource).Tag.ToString());
        }

        private void RunAction(String tag)
        {
            RemoteUIElementCustom();
            btn_Download.Style = (Style)this.Resources["StyleButtonMainFormUnChecked"];
            btn_Sounds.Style = (Style)this.Resources["StyleButtonMainFormUnChecked"];
            btn_Games.Style = (Style)this.Resources["StyleButtonMainFormUnChecked"];
            if (tag == "download")
            {
                btn_Download.Style = (Style)this.Resources["StyleButtonMainFormChecked"];
                container.RegisterType<Download.IDownloadControl, Download.DownloadControl>("o_download",
                    new Unity.Injection.InjectionConstructor((IMainWindow)this, appData));
                UIElement element = (UIElement)container.Resolve<Download.IDownloadControl>("o_download");
                AddUIElementCustom(element);
            }
            else if (tag == "sounds")
            {
                btn_Sounds.Style = (Style)this.Resources["StyleButtonMainFormChecked"];
                container.RegisterType<Sounds.ISoundsControl, Sounds.SoundsControl>("o_sounds",
                    new Unity.Injection.InjectionConstructor((IMainWindow)this, appData));
                UIElement element = (UIElement)container.Resolve<Sounds.ISoundsControl>("o_sounds");
                AddUIElementCustom(element);
            }
            else if (tag == "games")
            {
                btn_Games.Style = (Style)this.Resources["StyleButtonMainFormChecked"];
                container.RegisterType<Games.IGamesControl, Games.GamesControl>("o_games",
                    new Unity.Injection.InjectionConstructor((IMainWindow)this, appData));
                UIElement element = (UIElement)container.Resolve<Games.IGamesControl>("o_games");
                AddUIElementCustom(element);
            }
        }

        private void AddUIElementCustom(UIElement uiElement)
        {
            Grid.SetRow(uiElement, 0);
            Grid.SetColumn(uiElement, 1);
            GridMain.Children.Add(uiElement);
            GridMain.RegisterName(RegElementNameCustom, uiElement);
        }

        private void RemoteUIElementCustom()
        {
            object o = GridMain.FindName(RegElementNameCustom);
            if (o != null)
            {
                GridMain.Children.Remove(o as UIElement);
                UnregisterName(RegElementNameCustom);
            }
        }

        private void RemoteUIElementWait()
        {
            object o = GridMain.FindName(RegElementNameWait);
            if (o != null)
            {
                GridMain.Children.Remove(o as UIElement);
                UnregisterName(RegElementNameWait);
            }
        }

        private Boolean FlagShowWaitWindow;
        private Int32 CounterShowWaitWindow;

        private void BackgroundWorkerShowWaitWindowWithAutoCounter_DoWork(object sender, DoWorkEventArgs e)
        {
            int i = 0, j = 10;
            for (i = 0; i < 10000; i++)
            {
                j += 3;
                if (j > 100)
                    j = 0;
                ((Wait.IWaitControl)e.Argument).SetValueProgressBar(j);
                System.Threading.Thread.Sleep(300);
                if (!FlagShowWaitWindow)
                    break;
            }
        }

        private void BackgroundWorkerShowWaitWindowWithCounter_DoWork(object sender, DoWorkEventArgs e)
        {
            while (FlagShowWaitWindow) { System.Threading.Thread.Sleep(300); }
        }

        private void BackgroundWorkerShowWaitWindow_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // снимаем блокировку с контролов
            LockEnabledUserControls(false);

            RemoteUIElementWait();
            object o = GridMain.FindName(RegElementNameCustom);
            if (o != null)
            {
                ((UIElement)o).Visibility = Visibility.Visible;
            }
            sp_Buttons.Opacity = 1;
        }

        void IMainWindow.MainWindowWaitRun(Boolean WithAutoCounter)
        {
            MainWindowWaitRun(WithAutoCounter);
        }

        void IMainWindow.MainWindowWaitSetPB(Int32 counter)
        {
            MainWindowWaitSetPB(counter);
        }

        Int32 IMainWindow.MainWindowWaitGetPB()
        {
            return MainWindowWaitGetPB();
        }

        void IMainWindow.MainWindowWaitCancel()
        {
            MainWindowWaitCancel();
        }

        protected void MainWindowWaitSetPB(Int32 counter)
        {
            CounterShowWaitWindow = counter;
        }

        protected Int32 MainWindowWaitGetPB()
        {
            return CounterShowWaitWindow;
        }

        protected void MainWindowWaitRun(Boolean WithAutoCounter)
        {
            // флаг
            FlagShowWaitWindow = true;

            sp_Buttons.Opacity = 0.2;
            container.RegisterType<Wait.IWaitControl, Wait.WaitControl>("o_wait");
            UIElement element = (UIElement)container.Resolve<Wait.IWaitControl>("o_wait");
            Grid.SetRow(element, 0);
            Grid.SetColumn(element, 1);
            GridMain.Children.Add(element);
            GridMain.RegisterName(RegElementNameWait, element);

            // устанавливаем блокировку контролов
            LockEnabledUserControls(true);

            if (WithAutoCounter)
                backgroundWorkerShowWaitWindowWithAutoCounter.RunWorkerAsync(element);
            else
                backgroundWorkerShowWaitWindowWithCounter.RunWorkerAsync(element);
        }

        void IMainWindow.MainWindowUserWaitRun()
        {
            MainWindowUserWaitRun();
        }

        protected void MainWindowUserWaitRun()
        {
            sp_Buttons.Opacity = 0.2;
            LockEnabledUserControls(true);
        }

        void IMainWindow.MainWindowUserWaitCancel()
        {
            MainWindowUserWaitCancel();
        }

        protected void MainWindowUserWaitCancel()
        {
            sp_Buttons.Opacity = 1;
            LockEnabledUserControls(false);
        }

        protected void MainWindowWaitCancel()
        {
            FlagShowWaitWindow = false;
        }

        private void LockEnabledUserControls(Boolean lock_controls)
        {
           sp_Buttons.IsEnabled = !lock_controls;
        }

        void IMainWindow.MainWindowMessageShow(String message)
        {
            MainWindowMessageShow(message);
        }

        protected void MainWindowMessageShow(String message)
        {
            sp_Buttons.Opacity = 0.2;
            LockEnabledUserControls(true);

            container.RegisterType<MessageShow.IMessageControl, MessageShow.MessageControl>("o_message");
            UIElement element = (UIElement)container.Resolve<MessageShow.IMessageControl>("o_message");
            Grid.SetRow(element, 0);
            Grid.SetColumn(element, 1);
            GridMain.Children.Add(element);
            GridMain.RegisterName(RegElementNameError, element);

            ((MessageShow.IMessageControl)element).SetValues(this, message);
        }

        private void RemoteUIElementError()
        {
            object o = GridMain.FindName(RegElementNameError);
            if (o != null)
            {
                GridMain.Children.Remove(o as UIElement);
                UnregisterName(RegElementNameError);
            }
        }

        void IMainWindow.MainWindowMessageCancel()
        {
            MainWindowMessageCancel();
        }

        protected void MainWindowMessageCancel()
        {
            RemoteUIElementError();
            object o = GridMain.FindName(RegElementNameCustom);
            if (o != null)
            {
                ((UIElement)o).Visibility = Visibility.Visible;
            }
            sp_Buttons.Opacity = 1;
            LockEnabledUserControls(false);
        }
    }
}