// -- FILE ------------------------------------------------------------------
// name       : ConverterGridViewColumn.cs
// created    : Jani Giannoudis - 2008.03.27
// language   : c#
// environment: .NET 3.0
// copyright  : (c) 2008-2012 by Itenso GmbH, Switzerland
// --------------------------------------------------------------------------
using System;
using System.Windows.Data;
using System.Windows.Controls;
using System.Globalization;

namespace Itenso.Windows.Controls.ListViewLayout
{

	// ------------------------------------------------------------------------
	public abstract class ConverterGridViewColumn : GridViewColumn, IValueConverter
	{

		// ----------------------------------------------------------------------
		protected ConverterGridViewColumn( Type bindingType )
		{
            this.bindingType = bindingType ?? throw new ArgumentNullException( "bindingType" );

            // binding
            Binding binding = new Binding
            {
                Mode = BindingMode.OneWay,
                Converter = this
            };
            DisplayMemberBinding = binding;
		} // ConverterGridViewColumn

		// ----------------------------------------------------------------------
		public Type BindingType
		{
			get { return bindingType; }
		} // BindingType

		// ----------------------------------------------------------------------
		protected abstract object ConvertValue( object value );

		// ----------------------------------------------------------------------
		object IValueConverter.Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			if ( !bindingType.IsInstanceOfType( value ) )
			{
				throw new InvalidOperationException();
			}
			return ConvertValue( value );
		} // IValueConverter.Convert

		// ----------------------------------------------------------------------
		object IValueConverter.ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		} // IValueConverter.ConvertBack

		// ----------------------------------------------------------------------
		// members
		private readonly Type bindingType;

	} // class ConverterGridViewColumn

} // namespace Itenso.Windows.Controls.ListViewLayout
// -- EOF -------------------------------------------------------------------
